from django.urls import path
from receipts.views import receipt_list, add_receipt, create_account, account_list, create_category, category_list


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", add_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
